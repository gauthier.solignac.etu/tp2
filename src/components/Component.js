export default class Component {
  tagName;
  children;
  attribute;

  constructor(tagName, attribute, children) {
    this.tagName = tagName;
    this.children = children;
    this.attribute = attribute;
  }
  render() {
    return `
    <${this.tagName} ${this.renderAttribute()}>
      ${this.renderChildren()}
    </${this.tagName}>`;
  }

  renderChildren() {
    if (Array.isArray(this.children)) {
      return this.children.reduce((acc, child) => {
        return acc + this.renderChild(child);
      }, "");
    } else {
      return this.renderChild(this.children);
    }
  }
  renderAttribute() {
    if (this.attribute) {
      return `${this.attribute.name}="${this.attribute.value}"`;
    } else {
      return "";
    }
  }

  renderChild(child) {
    if (child instanceof Component) {
      return child.render();
    } else {
      return child;
    }
  }
}
