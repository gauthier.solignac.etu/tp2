import Component from "./Component";

export default class Img extends Component {
  constructor(src) {
    super("img", { name: "src", value: src }, null);
  }
  render() {
    return `<${this.tagName} ${this.attribute.name}=${this.attribute.value}/>`;
  }
}
