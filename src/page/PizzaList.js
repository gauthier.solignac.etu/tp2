import Component from "../components/Component";
import PizzaThumbnail from "../components/PizzaThumbnail";

export default class PizzaList extends Component {
  #pizzas;

  constructor(pizzas) {
    super(
      "section",
      { name: "class", value: "pizzaList" },
      pizzas.map((pizza) => {
        return new PizzaThumbnail(pizza);
      })
    );
    this.#pizzas = pizzas;
  }

  set pizzas(pizzas) {
    this.#pizzas = pizzas;
  }
  get pizzas() {
    return this.#pizzas;
  }
}
