import Component from "./components/Component";
export default class Router {
  static titleElement;
  static contentElement;
  static routes;
  static navigate(path) {
    //console.log(this.routes[0].page.pizzas);
    this.routes.forEach((element) => {
      if (element.path == path) {
        const c = new Component("h1", null, element.title);
        this.titleElement.innerHTML = c.render();
        console.log(element.page.pizzas);
        this.contentElement.innerHTML = element.page.render();
      }
    });
  }
}
