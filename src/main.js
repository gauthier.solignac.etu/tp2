import Component from "./components/Component.js";
import Img from "./components/Img.js";
import data from "./data.js";
import PizzaThumbnail from "./components/PizzaThumbnail.js";
import PizzaList from "./page/PizzaList.js";
import Router from "./Router";

//const titletest = new Component("h1", null, "La carte");
//document.querySelector(".pageTitle").innerHTML = title.render();
//console.log(titletest.render());

/*const title = new Component("h1", null, ["La", " ", "carte"]);
document.querySelector(".pageTitle").innerHTML = title.render();

/*const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector(".pageContent").innerHTML = pizzaThumbnail.render();

// `data` est le tableau défini dans `src/data.js`

//document.querySelector(".pageContent").innerHTML = pizzaList.render();*/
//const pizzaList = new PizzaList(data);

Router.titleElement = document.querySelector(".pageTitle");
Router.contentElement = document.querySelector(".pageContent");
/*Router.routes = [{ path: "/", page: pizzaList, title: "La carte" }];
Router.navigate("/");*/

const pizzaList = new PizzaList([]);
Router.routes = [{ path: "/", page: pizzaList, title: "La carte" }];

//Router.navigate("/"); // affiche une page vide
pizzaList.pizzas = data;

Router.navigate("/"); // affiche la liste des pizzas
